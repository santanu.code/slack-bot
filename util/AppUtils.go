package util

import (
	"github.com/labstack/gommon/log"
)

func HError(e error) {
	if e != nil {
		log.Error("Got an error: " + e.Error())
		return
	}
}
