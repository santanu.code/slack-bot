package service

import (
	"golang.org/x/net/context"
	"slack-bot/models"
	"slack-bot/repo"
	"slack-bot/util"
)

// BotToken Init Variables required Later
var BotToken = "BOT_TOKEN"
var ChannelId = "CHANNEL_ID"

func InitBotValues() {
	db := repo.GetDBConn()
	utilKVs := make(map[string]string)
	botDetailsList := make([]models.BotMeta, 0)
	err := db.NewSelect().Model(&botDetailsList).Column("key_name", "key_value").Scan(context.Background())
	util.HError(err)
	for _, botDetail := range botDetailsList {
		utilKVs[botDetail.Key] = botDetail.Value
	}
	// Populate Init Variables required Later
	BotToken = utilKVs[BotToken]
	ChannelId = utilKVs[ChannelId]
}
