package main

import (
	"fmt"
	"slack-bot/http"
	"slack-bot/service"
)

func main() {
	fmt.Println("Starting Slack Bot..")
	service.InitBotValues()
	http.InitRoutes()
}
