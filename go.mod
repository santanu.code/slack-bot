module slack-bot

go 1.16

require (
	github.com/go-sql-driver/mysql v1.7.0
	github.com/labstack/echo/v4 v4.5.0
	github.com/labstack/gommon v0.3.0
	github.com/slack-go/slack v0.9.4
	github.com/uptrace/bun v1.1.10
	github.com/uptrace/bun/dialect/mysqldialect v1.1.10
	golang.org/x/net v0.5.0
)
