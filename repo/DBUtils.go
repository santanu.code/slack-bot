package repo

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/mysqldialect"
	"slack-bot/util"
)

const User = "sql12595419"
const pwd = "6PFCISEeWC"
const Db = "sql12595419"
const Host = "sql12.freesqldatabase.com"

func GetDBConn() *bun.DB {
	sqldb, err := sql.Open("mysql", dsn(Db, User, pwd, Host))
	util.HError(err)
	db := bun.NewDB(sqldb, mysqldialect.New())
	return db
}

func dsn(dbName string, un string, pwd string, host string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", un, pwd, host, dbName)
}
